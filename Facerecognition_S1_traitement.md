# Fonctionnalité 2 : Effectuer un traitement sur une image et afficher le résultat du traitement


Nous allons maintenant effectuer un traitement simple sur une image. Ecrivez une foncion `def process_image(filename)` qui prend une image, effectue un traitement de votre choix sur cette image et affiche l'image résultant du traitement.


Une liste des traitements possibles est [ici](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_table_of_contents_imgproc/py_table_of_contents_imgproc.html).


Nous avons maintenant fini notre fonctionnalité de prise en main d'OpenCV. Il faut donc avant de passer à la suite :

+ <span style='color:blue'>Faire un commit sur votre dépôt local</span> 
+ <span style='color:blue'>Pousser (Push) le code vers le  dépôt distant sur GitLab.</span> 


Nous pouvons maintenant passer à la fonctionnalité [**Fonctionnalité 3** : Structuration et constitution de la base de données](./Facerecognition_S2_database.md) 


