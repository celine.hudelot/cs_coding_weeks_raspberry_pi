# Fonctionnalité 3  : Structuration et constitution de la base de données


Tout est dit dans le titre ! 
Appliquez la méthodologie vue jusqu'alors pour developper cette fonctionnalité. Il faudra nous rendre un document avec votre découpage en étapes.

Petite précision. Ici nous n'attendons pas une base de données de type SQL. Il faut reflechir à comment nommer et structurer vos fichiers sur disque pour pouvoir assicier une image à la personne qu'elle représente au moment de l'apprentissage.


